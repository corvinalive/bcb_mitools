#ifndef mitools_unitH
#define mitools_unitH
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#include <windows.h>
#include <math.h>
#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

#pragma argsused
enum TFunctionResult {frOk=0,frErrorArg=1};
struct TProduct
   {
   char ProductName[36];
   char Origin[255];
   bool CanRecalc;
   };

typedef TFunctionResult (*fGetBetta)(double t, double P, double Dens, double &value);
typedef TFunctionResult (*fGetF)(double t, double P, double Dens, double &value);
typedef TFunctionResult (*fReCalc)(double t, double P, double Dens, double t1, double P1, /*out*/double& Dtp, double& Betta, double& F);


class TMITools
{
public:
   const ProductCount;
   double  K0[4];
   double  K1[4];
   TMITools(): ProductCount(6)
      {
      K0[0]=613.97226;
      K0[1]=346.42278;
      K0[2]=594.54180;
      K0[3]=186.96960;

      K1[0]=0;
      K1[1]=0.43884;
      K1[2]=0;
      K1[3]=0.48618;
      }
   TFunctionResult Student095(int n,double &value);
   int GetProductCount();
   void GetProductList(TProduct* Products);
   void GetProductList2(TList* ProductList);
   TFunctionResult GetBetta0(double t, double P,
                                    double Dens, double &value);
   TFunctionResult GetF0(double t, double P,
                                    double Dens, double &value);
   TFunctionResult GetBetta1(double t, double P,
                                    double Dens, double &value);
   TFunctionResult GetF1(double t, double P, double Dens, double &value);
   TFunctionResult Student099(int n,double &value);

   double a15(double Dens15, int Index);
   double at(double a15,double t);
   double f(double Dens15, double t);
   double Denstp(double Dens15, double t, double P, int Index);

   struct TOut
      {
      double r15;
      double a15;
      double Rtp;
      double atp;
      double ftp;
      };
   bool GetDens(double Dens, double t, double P, double t_to, double P_to,TOut* result, int Index);
   TFunctionResult ReCalc2(double t, double P, double Dens, double t1,    //�����
                double P1, /*out*/double& Dtp, double& Betta, double& F);
   TFunctionResult GetBetta2(double t, double P,
                                    double Dens, double &value);
   TFunctionResult GetF2(double t, double P,
                                    double Dens, double &value);
   TFunctionResult ReCalc3(double t, double P, double Dens, double t1,    //������
                double P1, /*out*/double& Dtp, double& Betta, double& F);
   TFunctionResult GetBetta3(double t, double P,
                                    double Dens, double &value);
   TFunctionResult GetF3(double t, double P,
                                    double Dens, double &value);
   TFunctionResult ReCalc4(double t, double P, double Dens, double t1,    //���������� �������
                double P1, /*out*/double& Dtp, double& Betta, double& F);
   TFunctionResult GetBetta4(double t, double P,
                                    double Dens, double &value);
   TFunctionResult GetF4(double t, double P,
                                    double Dens, double &value);
   TFunctionResult ReCalc5(double t, double P, double Dens, double t1,    //�����
                double P1, /*out*/double& Dtp, double& Betta, double& F);
   TFunctionResult GetBetta5(double t, double P,
                                    double Dens, double &value);
   TFunctionResult GetF5(double t, double P,
                                    double Dens, double &value);

   TFunctionResult GetBetta(int ProductNumber, double t, double P,
                                    double Dens, double &value);
   TFunctionResult GetF(int ProductNumber, double t, double P,
                                    double Dens, double &value);
   TFunctionResult ReCalc(int ProductNumber, double t, double P, double Dens, double t1,    //�����
                double P1, /*out*/double& Dtp, double& Betta, double& F);

};
//---------------------------------------------------------------------------
#endif
