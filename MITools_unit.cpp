//---------------------------------------------------------------------------
#include <windows.h>
#include <math.h>
#include <vcl.h>
#pragma hdrstop
#include "MITools_unit.h"
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


//TMITools::ProductCount=6;
//TMITools::K0[4]={613.97226, 346.42278, 594.54180, 186.96960};
//TMITools::K1[4]={0,         0.43884,   0,         0.48618};

TFunctionResult TMITools::Student095(int n,double &value)
{
// n - ���-�� ���������
const ValCount=33;
const int n1[]    ={1,      2,     3,     4,     5,     6,     7,     8,     9,     10,    11,
              12,     13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
              23,     24,    25,    26,    27,    28,    29,    30,    40,    60,    120};
const double val[]={12.706, 4.303, 3.182, 2.776, 2.571, 2.447, 2.365, 2.306, 2.262, 2.228, 2.201,
               2.179, 2.160, 2.145, 2.131, 2.120, 2.110, 2.101, 2.093, 2.086, 2.080, 2.074,
               2.069, 2.064, 2.060, 2.056, 2.052, 2.048, 2.045, 2.042, 2.021, 2.000, 1.980};
   int i=n-1;
   if(n<=1)
      return frErrorArg;
   int index=ValCount-1;
   while( i<n1[index] )
      index--;
   value=val[index];
   return frOk;
}
//---------------------------------------------------------------------------
int TMITools::GetProductCount()
{
   return ProductCount;
}
//---------------------------------------------------------------------------
void TMITools::GetProductList(TProduct* Products)
{
   memset(Products,0,ProductCount*sizeof(TProduct));
   strcpy(Products->ProductName,"�����");
   strcpy(Products->Origin,"����� ��������. ������� �������� �� ��2153-2001");
   Products->CanRecalc=false;

   Products++;
   strcpy(Products->ProductName,"����");
   strcpy(Products->Origin,"����. Betta=0.000260 F=0.00000491");
   Products->CanRecalc=false;

   Products++;
   strcpy(Products->ProductName,"�����");
   strcpy(Products->Origin,"�������� �����. ���������� �� �� 2632-2001");
   Products->CanRecalc=true;

   Products++;
   strcpy(Products->ProductName,"������");
   strcpy(Products->Origin,"������. ���������� �� �� 2632-2001");
   Products->CanRecalc=true;

   Products++;
   strcpy(Products->ProductName,"���������� �������");
   strcpy(Products->Origin,"���������� �������. ���������� �� �� 2632-2001");
   Products->CanRecalc=true;

   Products++;
   strcpy(Products->ProductName,"�����");
   strcpy(Products->Origin,"�����. ���������� �� �� 2632-2001");
   Products->CanRecalc=true;
   return;
}
//---------------------------------------------------------------------------
void TMITools::GetProductList2(TList* ProductList)
{
   int c=ProductList->Count;
   TProduct* Products;
   if(c>=1)
      {
      Products=(TProduct*)ProductList->Items[0];
      memset(Products,0,sizeof(TProduct));
      strcpy(Products->ProductName,"�����");
      strcpy(Products->Origin,"����� ��������. ������� �������� �� ��2153-2001");
      Products->CanRecalc=false;
      }
   else
      return;

   if(c>=2)
      {
      Products=(TProduct*)ProductList->Items[1];
      memset(Products,0,sizeof(TProduct));
      strcpy(Products->ProductName,"����");
      strcpy(Products->Origin,"����. Betta=0.000260 F=0.00000491");
      Products->CanRecalc=false;
      }
   else
      return;

   if(c>=3)
      {
      Products=(TProduct*)ProductList->Items[2];
      memset(Products,0,sizeof(TProduct));
      strcpy(Products->ProductName,"�����");
      strcpy(Products->Origin,"�������� �����. ���������� �� �� 2632-2001");
      Products->CanRecalc=true;
      }
   else
      return;

   if(c>=4)
      {
      Products=(TProduct*)ProductList->Items[3];
      memset(Products,0,sizeof(TProduct));
      strcpy(Products->ProductName,"������");
      strcpy(Products->Origin,"������. ���������� �� �� 2632-2001");
      Products->CanRecalc=true;
      }
   else
      return;

   if(c>=5)
      {
      Products=(TProduct*)ProductList->Items[4];
      memset(Products,0,sizeof(TProduct));
      strcpy(Products->ProductName,"���������� �������");
      strcpy(Products->Origin,"���������� �������. ���������� �� �� 2632-2001");
      Products->CanRecalc=true;
      }
   else
      return;

   if(c>=6)
      {
      Products=(TProduct*)ProductList->Items[5];
      memset(Products,0,sizeof(TProduct));
      strcpy(Products->ProductName,"�����");
      strcpy(Products->Origin,"�����. ���������� �� �� 2632-2001");
      Products->CanRecalc=true;
      }
   else
      return;
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetBetta0(double t, double P,
                                    double Dens, double &value)
{
   const double val[65][9]=
      {
         {1.082, 1.080,	1.078, 1.076, 1.073, 1.071, 1.068, 1.066,	1.063},
         {1.054, 0.052, 1.050, 1.048, 1.046, 1.043, 1.041, 1.038, 1.036},
         {1.027, 1.025, 1.023, 1.021, 1.019, 1.017, 1.014, 1.012, 1.009},
         {1.001, 0.999, 0.997, 0.995, 0.993, 0.991, 0.989, 0.987, 0.984},
         {0.976, 0.974, 0.972, 0.970, 0.969, 0.966, 0.964, 0.962, 0.960},
         {0.961, 0.960, 0.958, 0.956, 0.954, 0.952, 0.950, 0.948, 0.946},
         {0.956, 0.955, 0.953, 0.951, 0.949, 0.947, 0.945, 0.943, 0.941},
         {0.952, 0.95, 0.948, 0.947, 0.945, 0.943, 0.941, 0.939, 0.936},
         {0.947, 0.945, 0.944, 0.942, 0.94, 0.938, 0.936, 0.934, 0.932},
         {0.942, 0.941, 0.939, 0.937, 0.936, 0.933, 0.931, 0.929, 0.927},
         {0.938, 0.936, 0.934, 0.933, 0.931, 0.929, 0.927, 0.925, 0.923},
         {0.933, 0.931, 0.93, 0.928, 0.926, 0.924, 0.922, 0.92, 0.918},
         {0.928, 0.927, 0.925, 0.923, 0.922, 0.92, 0.918, 0.916, 0.914},
         {0.924, 0.922, 0.921, 0.919, 0.917, 0.915, 0.913, 0.912, 0.91},
         {0.919, 0.918, 0.916, 0.914, 0.913, 0.911, 0.909, 0.907, 0.905},
         {0.915, 0.913, 0.912, 0.91, 0.908, 0.907, 0.905, 0.903, 0.901},
         {0.91, 0.909, 0.907, 0.906, 0.904, 0.902, 0.9, 0.898, 0.896},
         {0.906, 0.904, 0.903, 0.901, 0.9, 0.898, 0.896, 0.894, 0.892},
         {0.902, 0.9, 0.899, 0.897, 0.895, 0.893, 0.892, 0.89, 0.888},
         {0.897, 0.896, 0.894, 0.893, 0.891, 0.889, 0.887, 0.886, 0.884},
         {0.893, 0.891, 0.89, 0.888, 0.887, 0.885, 0.883, 0.881, 0.879},
         {0.889, 0.887, 0.886, 0.884, 0.882, 0.881, 0.879, 0.877, 0.875},
         {0.884, 0.883, 0.881, 0.88, 0.878, 0.877, 0.875, 0.873, 0.871},
         {0.88, 0.879, 0.877, 0.876, 0.874, 0.872, 0.871, 0.869, 0.867},
         {0.876, 0.874, 0.873, 0.871, 0.87, 0.868, 0.867, 0.865, 0.863},
         {0.872, 0.87, 0.869, 0.867, 0.866, 0.864, 0.962, 0.861, 0.859},
         {0.867, 0.866, 0.865, 0.863, 0.862, 0.86, 0.858, 0.857, 0.855},
         {0.863, 0.862, 0.861, 0.859, 0.858, 0.856, 0.854, 0.853, 0.851},
         {0.859, 0.858, 0.857, 0.855, 0.854, 0.852, 0.85, 0.849, 0.847},
         {0.855, 0.854, 0.853, 0.851, 0.85, 0.848, 0.846, 0.845, 0.843},
         {0.851, 0.85, 0.849, 0.847, 0.846, 0.844, 0.842, 0.841, 0.839},
         {0.847, 0.846, 0.845, 0.843, 0.842, 0.84, 0.838, 0.837, 0.835},
         {0.843, 0.842, 0.841, 0.839, 0.838, 0.836, 0.835, 0.833, 0.831},
         {0.839, 0.838, 0.837, 0.835, 0.834, 0.832, 0.831, 0.829, 0.828},
         {0.835, 0.834, 0.833, 0.831, 0.83, 0.828, 0.827, 0.825, 0.824},
         {0.831, 0.83, 0.829, 0.828, 0.826, 0.825, 0.823, 0.822, 0.82},
         {0.828, 0.826, 0.825, 0.824, 0.822, 0.821, 0.819, 0.818, 0.816},
         {0.824, 0.823, 0.821, 0.82, 0.818, 0.817, 0.816, 0.814, 0.812},
         {0.82, 0.819, 0.817, 0.816, 0.815, 0.813, 0.812, 0.81, 0.809},
         {0.816, 0.815, 0.814, 0.812, 0.811, 0.81, 0.808, 0.807, 0.805},
         {0.812, 0.811, 0.81, 0.809, 0.807, 0.806, 0.804, 0.803, 0.801},
         {0.809, 0.807, 0.806, 0.805, 0.804, 0.802, 0.801, 0.799, 0.798},
         {0.805, 0.804, 0.803, 0.801, 0.8, 0.799, 0.797, 0.796, 0.794},
         {0.801, 0.8, 0.799, 0.798, 0.796, 0.795, 0.794, 0.972, 0.791},
         {0.798, 0.796, 0.795, 0.794, 0.793, 0.791, 0.79, 0.789, 0.787},
         {0.794, 0.793, 0.792, 0.79, 0.789, 0.788, 0.786, 0.785, 0.783},
         {0.79, 0.789, 0.788, 0.787, 0.786, 0.784, 0.783, 0.781, 0.78},
         {0.787, 0.786, 0.785, 0.783, 0.782, 0.781, 0.779, 0.778, 0.777},
         {0.783, 0.782, 0.781, 0.78, 0.778, 0.777, 0.776, 0.774, 0.773},
         {0.78, 0.779, 0.777, 0.776, 0.775, 0.774, 0.772, 0.771, 0.77},
         {0.776, 0.775, 0.774, 0.773, 0.772, 0.77, 0.769, 0.768, 0.766},
         {0.773, 0.772, 0.771, 0.769, 0.768, 0.767, 0.766, 0.764, 0.763},
         {0.769, 0.768, 0.767, 0.766, 0.765, 0.763, 0.762, 0.761, 0.759},
         {0.766, 0.765, 0.764, 0.762, 0.761, 0.76, 0.759, 0.757, 0.756},
         {0.762, 0.761, 0.76, 0.759, 0.758, 0.757, 0.755, 0.754, 0.753},
         {0.752, 0.751, 0.75, 0.749, 0.748, 0.747, 0.745, 0.744, 0.743},
         {0.736, 0.735, 0.734, 0.733, 0.732, 0.731, 0.729, 0.728, 0.727},
         {0.72, 0.719, 0.718, 0.717, 0.716, 0.715, 0.714, 0.713, 0.711},
         {0.705, 0.704, 0.703, 0.702, 0.701, 0.7, 0.699, 0.698, 0.696},
         {0.69, 0.689, 0.688, 0.687, 0.686, 0.685, 0.684, 0.683, 0.682},
         {0.675, 0.675, 0.674, 0.673, 0.672, 0.671, 0.67, 0.669, 0.668},
         {0.661, 0.661, 0.66, 0.659, 0.658, 0.657, 0.656, 0.655, 0.654},
         {0.648, 0.647, 0.646, 0.645, 0.645, 0.644, 0.643, 0.642, 0.641},
         {0.635, 0.634, 0.633, 0.632, 0.632, 0.631, 0.63, 0.629, 0.628},
         {0.622, 0.621, 0.621, 0.62, 0.619, 0.618, 0.617, 0.616, 0.616}
      };
   const ti[]={ 5, 10, 15, 20, 25, 30, 35, 40, 45};
   const di[]= {760,770,780,790,800,802,804,806,808,810,812,814,816,818,820,822,
                824,826,828,830,832,834,836,838,840,842,844,846,848,850,852,854,
                856,858,860,862,864,866,868,870,872,874,876,878,880,882,884,886,
                888,890,892,894,896,898,900,910,920,930,940,950,960,970,980,990,1000};


   if( (t<0) || (t>45) || (Dens<750) || (Dens>1000) )
      return frErrorArg;
   int i=0;
   while( (t >= ti[i]) && (i<8) )
      i++;
   int j=0;
   while( (Dens >= di[j])&& (j<64) )
      j++;
   value = val[j][i]/1000;
   return frOk;
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetF0(double t, double P,
                                    double Dens, double &value)
{
   const double val[][5]=
      {
         {0.9800,	1.0210,	1.0630,	1.1050,	1.1470},
         {0.9390,	0.9790,	1.0190,	1.0590,	1.0990},
         {0.9020,	0.9400,	0.9780,  1.0160,	1.0540},
         {0.8670,	0.9030,	0.9400,	0.9760,	1.0130},
         {0.8350,	0.8700,	0.9050,	0.9400,	0.9750},
         {0.8050,	0.8390,	0.8720,	0.9060,	0.9400},
         {0.7780,	0.8100,	0.8420,	0.8740,	0.9070},
         {0.7520,	0.7830,	0.8130,	0.8440,	0.8760},
         {0.7280,	0.7570,	0.7870,	0.8170,	0.8470},
         {0.7060,	0.7340,	0.7620,	0.7910,	0.8200},
         {0.6850,	0.7120,	0.7390,	0.7670,	0.7950},
         {0.6650,	0.6910,	0.7180,	0.7440,	0.7710},
         {0.6470,	0.6720 ,	0.6970,	0.7230,	0.7490},
         {0.6300,	0.6540,	0.6780,	0.7030,	0.7280},
         {0.6130,	0.6370,	0.6600,	0.6840,	0.7080},
         {0.5980,	0.6210,	0.6430,	0.6660,	0.6890},
         {0.5840,	0.6050,	0.6270,	0.6500,	0.6720},
         {0.5700,	0.5910,	0.6120,	0.6340,	0.6550},
         {0.5570,	0.5770,	0.5980,	0.6190,	0.6400},
         {0.5450,	0.5650,	0.5840,	0.6040,	0.6250},
         {0.5330,	0.5520,	0.5720,	0.5910,	0.6110},
         {0.5230,	0.5410,	0.5590,	0.5780,	0.5970},
         {0.5120,	0.5300,	0.5480,	0.5660,	0.5840},
         {0.5020,	0.5200,	0.5370,	0.5550,	0.5720},
         {0.4930,	0.5100,	0.5270,	0.5440,	0.5610}
      };
   const int ti[]={10,	20,	30,	40,	50};
   const int di[]={760,770,780,790,800,810,820,830,840,850,860,870,880,890,900,
                   910,920,930,940,950,960,970,980,990,1000};

   if( (t<0) || (t>50) || (Dens<750) || (Dens>1000) )
      return frErrorArg;
   int i=0;
   while( (t >= ti[i]) && (i<4) )
      i++;
   int j=0;
   while( (Dens >= di[j])&& (j<24) )
      j++;
   value = val[j][i]/1000;
   return frOk;
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetBetta1(double t, double P,
                                    double Dens, double &value)
{
   value=0.000260;
   return frOk;
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetF1(double t, double P,
                                    double Dens, double &value)
{
   value=0.000491;
   return frOk;
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::Student099(int n,double &value)
{
// n - ���-�� ���������
// n - ���-�� ���������
const ValCount=33;
const int n1[]    ={1,      2,     3,     4,     5,     6,     7,     8,     9,     10,    11,
              12,     13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
              23,     24,    25,    26,    27,    28,    29,    30,    40,    60,    120};
const double val[]={63.657, 9.925, 5.841, 4.604, 4.032, 3.707, 3.499, 3.355, 3.250, 3.169, 3.106,
                     3.055, 3.012, 2.977, 2.947, 2.921, 2.898, 2.878, 2.861, 2.845, 2.831, 2.819,
                     2.807, 2.797, 2.787, 2.779, 2.771, 2.763, 2.756, 2.750, 2.704, 2.660, 2.617};
   int i=n-1;
   if(n<=1)
      return frErrorArg;
   int index=ValCount-1;
   while( i<n1[index] )
      index--;
   value=val[index];
   return frOk;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
double TMITools::a15(double Dens15, int Index)
{
   return (K0[Index]+K1[Index]*Dens15)/(Dens15*Dens15);
}
//---------------------------------------------------------------------------
double TMITools::at(double a15,double t)
{
   return a15+1.6*a15*a15*(t-15);
}
//---------------------------------------------------------------------------
double TMITools::f(double Dens15, double t)
{
   return exp(-1.6208+0.00021592*t+((0.87096*1000000+4.2092*t*1000)/(Dens15*Dens15)) )/1000;
}
//---------------------------------------------------------------------------
double TMITools::Denstp(double Dens15, double t, double P, int Index)
{
   double a1=a15(Dens15,Index);
   double ff=f(Dens15,t);

   double dens=Dens15*exp(-1*a1*(t-15)*(1+0.8*a1*(t-15)))/(1-ff*P);
   return dens;
};
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
bool TMITools::GetDens(double Dens, double t, double P, double t_to, double P_to,TOut* result, int Index)
{
//Get r15;
   double rr=Dens;
   double r15=Dens;
   double aa15;
   double ff;
   double rr15;
   double dr;
   double d=0.0000001;
   int count=1000000;
   do
      {
      aa15=(K0[Index]+K1[Index]*r15)/(r15*r15);
      ff=f(r15,t);
      rr15=rr*(1-ff*P)/(exp( -aa15*(t-15)*(1+0.8*aa15*(t-15))) );
      dr=rr15-r15;
      r15=rr15;
      if(dr<0) dr*=-1;
      count--;
      if(count==0)
         return false;
      }
   while( dr>d);

   result->r15=r15;
   result->a15=a15(r15,Index);
   result->Rtp=Denstp(r15,t_to,P_to,Index);
   result->atp=at(result->a15,t_to);
   result->ftp=f(result->r15,t_to);
   return true;
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::ReCalc2(double t, double P, double Dens, double t1,    //�����
                double P1, /*out*/double& Dtp, double& Betta, double& F)
{
   TOut o;
   bool Ok=GetDens(Dens,t,P,t1,P1,&o,0);
   //return values
   Dtp=o.Rtp;
   Betta=o.atp;
   F=o.ftp;
   if(Ok)
      return frOk;
   else
      return frErrorArg;
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetBetta2(double t, double P,
                                    double Dens, double &value)
{
   double d;
   return ReCalc2(t,P,Dens,t,P,Dens,value,d);
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetF2(double t, double P,
                                    double Dens, double &value)
{
   double d;
   return ReCalc2(t,P,Dens,t,P,Dens,d,value);
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::ReCalc3(double t, double P, double Dens, double t1,    //������
                double P1, /*out*/double& Dtp, double& Betta, double& F)
{
   TOut o;
   bool Ok=GetDens(Dens,t,P,t1,P1,&o,1);
   //return values
   Dtp=o.Rtp;
   Betta=o.atp;
   F=o.ftp;
   if(Ok)
      return frOk;
   else
      return frErrorArg;
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetBetta3(double t, double P,
                                    double Dens, double &value)
{
   double d;
   return ReCalc3(t,P,Dens,t,P,Dens,value,d);
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetF3(double t, double P,
                                    double Dens, double &value)
{
   double d;
   return ReCalc3(t,P,Dens,t,P,Dens,d,value);
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::ReCalc4(double t, double P, double Dens, double t1,    //���������� �������
                double P1, /*out*/double& Dtp, double& Betta, double& F)
{
   TOut o;
   bool Ok=GetDens(Dens,t,P,t1,P1,&o,2);
   //return values
   Dtp=o.Rtp;
   Betta=o.atp;
   F=o.ftp;
   if(Ok)
      return frOk;
   else
      return frErrorArg;
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetBetta4(double t, double P,
                                    double Dens, double &value)
{
   double d;
   return ReCalc4(t,P,Dens,t,P,Dens,value,d);
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetF4(double t, double P,
                                    double Dens, double &value)
{
   double d;
   return ReCalc4(t,P,Dens,t,P,Dens,d,value);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
TFunctionResult TMITools::ReCalc5(double t, double P, double Dens, double t1,    //�����
                double P1, /*out*/double& Dtp, double& Betta, double& F)
{
   TOut o;
   bool Ok=GetDens(Dens,t,P,t1,P1,&o,3);
   //return values
   Dtp=o.Rtp;
   Betta=o.atp;
   F=o.ftp;
   if(Ok)
      return frOk;
   else
      return frErrorArg;
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetBetta5(double t, double P,
                                    double Dens, double &value)
{
   double d;
   return ReCalc5(t,P,Dens,t,P,Dens,value,d);
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetF5(double t, double P,
                                    double Dens, double &value)
{
   double d;
   return ReCalc5(t,P,Dens,t,P,Dens,d,value);
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetBetta(int ProductNumber, double t, double P,
                                    double Dens, double &value)
{
   if (ProductNumber >=ProductCount)
      return frErrorArg;

   if (ProductNumber < 0)
      return frErrorArg;

   switch (ProductNumber)
      {
      case 0:
         return GetBetta0(t, P, Dens, value);
      case 1:
         return GetBetta1(t, P, Dens, value);
      case 2:
         return GetBetta2(t, P, Dens, value);
      case 3:
         return GetBetta3(t, P, Dens, value);
      case 4:
         return GetBetta4(t, P, Dens, value);
      case 5:
         return GetBetta5(t, P, Dens, value);
      }
   return  frErrorArg;
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::GetF(int ProductNumber, double t, double P,
                                    double Dens, double &value)
{
   if (ProductNumber >=ProductCount)
      return frErrorArg;

   if (ProductNumber < 0)
      return frErrorArg;

   switch (ProductNumber)
      {
      case 0:
         return GetF0(t, P, Dens, value);
      case 1:
         return GetF1(t, P, Dens, value);
      case 2:
         return GetF2(t, P, Dens, value);
      case 3:
         return GetF3(t, P, Dens, value);
      case 4:
         return GetF4(t, P, Dens, value);
      case 5:
         return GetF5(t, P, Dens, value);
      }
   return  frErrorArg;
}
//---------------------------------------------------------------------------
TFunctionResult TMITools::ReCalc(int ProductNumber, double t, double P, double Dens, double t1,    //�����
                double P1, /*out*/double& Dtp, double& Betta, double& F)
{
   if (ProductNumber >=ProductCount)
      return frErrorArg;

   if (ProductNumber < 0)
      return frErrorArg;

   switch (ProductNumber)
      {
      case 0:
         return frErrorArg;
      case 1:
         return frErrorArg;
      case 2:
         return ReCalc2(t, P, Dens, t1, P1, Dtp, Betta, F);
      case 3:
         return ReCalc3(t, P, Dens, t1, P1, Dtp, Betta, F);
      case 4:
         return ReCalc4(t, P, Dens, t1, P1, Dtp, Betta, F);
      case 5:
         return ReCalc5(t, P, Dens, t1, P1, Dtp, Betta, F);
      }
   return  frErrorArg;
}
//---------------------------------------------------------------------------


