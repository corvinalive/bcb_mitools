//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop


#include <math.hpp>
#include "variable.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

TColor TVar::Colors[VarColorCount];
//---------------------------------------------------------------------------
double Man10(double x, int& x10)
{
   x10=0;
   if(x==0) return 0;
   bool Neg=(x<0);
   double wx=x;
   if(x<0)  wx*=-1;
   if(wx>=1)
      do
         {
         wx/=10;
         x10++;
         }
      while(wx>1);
   else
      if(wx<0.1)
         do
            {
            wx*=10;
            x10--;
            }
         while(wx<0.1);
   if(Neg) wx*=-1;
   return wx;
}
//---------------------------------------------------------------------------
void __fastcall TVarOptions::LoadFromIni(TIniFile* ini,AnsiString s)
{
#pragma warn -8018
   Type=ini->ReadInteger(s,"VarType",0);
   Kind=ini->ReadInteger(s,"VarKind",0);

   CalcOptions.CalcRoundType = ini->ReadInteger(s,"CalcRoundType",0);
   CalcOptions.ShowRoundType = ini->ReadInteger(s,"ShowRoundType",0);
   CalcOptions.ShowRoundNumber = ini->ReadInteger(s,"ShowRoundNumber",0);
   CalcOptions.CalcRoundNumber = ini->ReadInteger(s,"CalcRoundNumber",0);
#pragma warn +8018
   Name=ini->ReadString(s,"Name","");
   Description=ini->ReadString(s,"Description","");
   Unit=ini->ReadString(s,"Unit","");
   Width = ini->ReadInteger(s,"Width",24);
   Bitmap = ini->ReadInteger(s,"Bitmap",-1);
   UnitBitmap = ini->ReadInteger(s,"UnitBitmap",-1);
};
//---------------------------------------------------------------------------
void __fastcall TVarOptions::FormatStr(TVar* Var,AnsiString& sText)
{
   switch (Type)
   {
     case Integer:
         sText=Var->Value.i;
         break;
     case Float:
#pragma warn -8006
         if(CalcOptions.ShowRoundType==TCalcOptions::None)
            sText=Var->Value.d;
         else
            if(CalcOptions.ShowRoundType==TCalcOptions::AfterDot)
               sText=FloatToStrF(Var->Value.d,Sysutils::ffNumber,1000000,
                                                   CalcOptions.ShowRoundNumber);
            else
               {//�������� ����
               int x10;
               double wx=Man10(Var->Value.d,x10);
               wx=RoundTo(wx,-(CalcOptions.ShowRoundNumber));
               wx*=Power(10,x10);
               int at=CalcOptions.ShowRoundNumber-x10;
               if(at<0) at=0;
               sText=FloatToStrF(wx,AnsiString::sffNumber,1000000,at);
               }
#pragma warn -8006
          break;
     case Bool:
         if(Var->Value.b)
            sText="��";
         else
            sText="���";
         break;
     case List:
         if(Strings)
            sText=Strings->Strings[Var->Value.i];
     break;
     case Text:
         sText=Var->Value.c;
     break;
   }
#pragma warn +8006
}
//---------------------------------------------------------------------------
bool __fastcall TGridData::CanEdit(TMICellData* Data)
{
   return (Data->vo->Kind!=TVarOptions::CalculatedData);
}
//---------------------------------------------------------------------------
void __fastcall TIntList::Save(IStream* Stream)
{
   int c=FList->Count;
   int j;
   Stream->Write(&c,sizeof(c),0);
   for(int i=0;i<c;i++)
      {
      j=Values[i];
      Stream->Write(&j,sizeof(j),0);
      }
}
//---------------------------------------------------------------------------
void __fastcall TIntList::Load(IStream* Stream)
{
   FList->Clear();
   int c;
   Stream->Read(&c,sizeof(c),0);
   FList->Count=c;
   /*Old variant
   int j;
   for(int i=0;i<c;i++)
      {
      Stream->Read(&j,sizeof(j),0);
      Values[i]=j;
      }
   */
   if(c==0)
      return;
   int* buf=new int[c];
   Stream->Read(buf,c*sizeof(int),0);
   for(int i=0;i<c;i++)
      Values[i]=buf[i];
   delete[] buf;
}
//---------------------------------------------------------------------------
void __fastcall TVarOptions::Save(IStream* Stream)
{
   Stream->Write(&Type,sizeof(TVarType),0);
   Stream->Write(&Kind,sizeof(TVarKind),0);
   Stream->Write(&CalcOptions,sizeof(TCalcOptions),0);
   Stream->Write(&Width,sizeof(int),0);
};
//---------------------------------------------------------------------------
void __fastcall TVarOptions::Load(IStream* Stream)
{
   Stream->Read(&Type,sizeof(TVarType),0);
   Stream->Read(&Kind,sizeof(TVarKind),0);
   Stream->Read(&CalcOptions,sizeof(TCalcOptions),0);

   Stream->Read(&Width,sizeof(int),0);
};
//---------------------------------------------------------------------------
__fastcall TGridData::TGridData(TMICommonData* MICommonData)
{
   FMICommonData = MICommonData;
   FOnChangeItemsStructure=NULL;
   FOnChange=NULL;
}
//---------------------------------------------------------------------------
void __fastcall TVarOptions::ProcessRound(TVar* Var)
{
   if(CalcOptions.CalcRoundType==TCalcOptions::None) return;
   if(CalcOptions.CalcRoundType==TCalcOptions::AfterDot)
      {
      Var->Value.d=RoundTo(Var->Value.d,-CalcOptions.CalcRoundNumber);
      }
   else
      {
      int x10;
      double wx=Man10(Var->Value.d,x10);
      Var->Value.d=RoundTo(wx,-(CalcOptions.CalcRoundNumber));
      Var->Value.d*=Power(10,x10);
      }
}
//---------------------------------------------------------------------------
void __fastcall TIntList::Assign(TIntList* Source)
{
	int c = Source->Count;
	FList->Count=c;
	for(int i=0;i<c;i++)
		Values[i]=Source->Values[i];
}
//---------------------------------------------------------------------------
void __fastcall TIntList::Exclude(TIntList* Excluded)
{
	int c = Excluded->Count;
	int val;
	int cc;
	for(int i=0;i<c;i++)
		{
		val=Excluded->Values[i];
		cc=Count;
		for(int j=0;j<cc;j++)
			{
			if(val==Values[j])
				{
				Delete(j);
				break;
				}
			}
		}
}
//---------------------------------------------------------------------------
void __fastcall TIntList::Move(int CurIndex, int NewIndex)
{
   FList->Move(CurIndex,NewIndex);
}
//---------------------------------------------------------------------------
int __fastcall TIntList::IndexOf(int Item)
{
   pi.i=Item;
   return FList->IndexOf(pi.p);
}
//---------------------------------------------------------------------------
int __fastcall TIntList::Remove(int Item)
{
   pi.i=Item;
   return FList->Remove(pi.p);
}
//---------------------------------------------------------------------------
AnsiString __fastcall TVarOptions::FormatSD(double value, int WantedDigits/*,AnsiString& result*/)
{
/*
   Description: ����������� ������ � �������� ���������� �������� ����. ���� ��� ���� ��������
   �����-�� �����, �� ���������� �������� ���� ������������� �� ����, ���� �� ����� ��� �����.
*/
   AnsiString result, ss;
   int prc=WantedDigits;
   int c;
   int x10;
   double wx;
   wx=Man10(value,x10);
   wx=RoundTo(wx,-(prc));
   wx*=Power(10,x10);
   int at=prc-x10;
   if(at<0)at=0;
   result=FloatToStrF(wx,Sysutils::ffNumber,1000000,at);
   ss=result;
   c=ss.Length();
   for(int i=c;i>0;i--)
      {
      if(ss[i]==ThousandSeparator)
         ss.Delete(i,1);
      }
   double check=ss.ToDouble();
   while(check!=value)
      {
      prc++;
      wx=Man10(value,x10);
      wx=RoundTo(wx,-(prc));
      wx*=Power(10,x10);
      at++;
      result=FloatToStrF(wx,Sysutils::ffNumber,1000000,at);
      ss=result;
      c=ss.Length();
      for(int i=c;i>0;i--)
         {
         if(ss[i]==ThousandSeparator)
            ss.Delete(i,1);
         }
      check=ss.ToDouble();
      }
   return result;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TVarOptions::FormatDAD(double value, int WantedDigits/*,AnsiString& result*/)
{
/*
   Description: ����������� ������ � �������� ���������� ���� ����� �������. ���� ��� ���� ��������
   �����-�� �����, �� ���������� ���� ����� ������� ������������� �� ����, ���� �� ����� ��� �����.
*/
   AnsiString result, ss;
   int prc=WantedDigits;
   result=FloatToStrF(value,Sysutils::ffNumber,1000000,prc);
   ss=result;
   int c;
   c=ss.Length();
   for(int i=c;i>0;i--)
      {
      if(ss[i]==ThousandSeparator)
         ss.Delete(i,1);
      }
   double check=ss.ToDouble();
   while(check!=value)
      {
      prc++;
      result=FloatToStrF(value,Sysutils::ffNumber,1000000,prc);
      ss=result;
      c=ss.Length();
      for(int i=c;i>0;i--)
         {
         if(ss[i]==ThousandSeparator)
            ss.Delete(i,1);
         }
      check=ss.ToDouble();
      }
   return result;
}
//---------------------------------------------------------------------------

